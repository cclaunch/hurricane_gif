#!/usr/bin/env python3
#
# Copyright 2021 Chuck Claunch
#
# A script to grab the National Hurricane Center's (NHC) forecast images and turn them into an animated GIF
#
# This script scrapes the NHC archive site for avaialble 5-day forecasts within the user given or default
# time period, downloads the images to a local folder, then creates an animated GIF out of them based on
# user given or default paramaters.  Pass `--help` to the script to see its usage.
#
# License is hereby given to do whatever the heck you want with this code, but it'd be nice if you left
# this comment block in or at least linked back to this repository for credit.
#
# If you really enjoyed it, grab me a coffee: https://www.buymeacoffee.com/chux
# If you want to see my other projects: https://chux.gitlab.io/

import argparse
import datefinder
import datetime
import os.path
import urllib.request
from PIL import Image


def main(date_start, date_end, basin, duration, end_frames):
    """Main function to grab the images and create the GIF."""
    # Because NHC for whatever reason uses epac and pac in the same URLs.
    basin_alt = basin
    if basin == 'epac':
        basin_alt = 'pac'

    page = urllib.request.urlopen(
        'https://www.nhc.noaa.gov/archive/xgtwo/gtwo_archive_list.php?basin={basin}'.format(basin=basin))
    contents = page.read().decode(page.headers.get_content_charset())

    # All of the archive dates are between these tags.
    start = contents.find('<!-- START OF CONTENT -->')
    end = contents.find('<!-- END OF CONTENT -->')
    datestring = contents[start:end]
    matches = datefinder.find_dates(datestring)

    # Only grab the dates we're looking for.
    dates = []
    for match in matches:
        if date_start <= match <= date_end:
            dates.append(match)

    # If these were out of order the GIF would look kinda dumb.
    dates.sort()

    # If the user provided an end date that was more than a day ago we don't
    # want to tack on the 'latest' image from NHC.
    add_latest = False
    if (datetime.datetime.now() - dates[-1]).total_seconds() <= 60 * 60 * 24:
        add_latest = True

    # Get the dates as a string formatted the way the NHC URL wants them.
    dates_stripped = [d.strftime('%Y%m%d%H%M') for d in dates]

    # Add the latest NHC forecast if needed.
    if add_latest:
        dates_stripped.append('latest')

    # Duplicate the final frame to make the gif "pause" on the final image
    # before restarting.
    for _ in range(end_frames):
        dates_stripped.append(dates_stripped[-1])

    # Keep the downloaded images in a temporary directory.
    tmp_dir = 'tmp_{basin}'.format(basin=basin)
    if not os.path.exists(tmp_dir):
        os.makedirs(tmp_dir)

    # Iterate the list to generate the URLs, download the files if needed, and
    # create the final file list.
    files = []
    for report_date in dates_stripped:
        url = 'https://www.nhc.noaa.gov/archive/xgtwo/{basin}/{report_date}/two_{basin_alt}_5d0.png'.format(
            basin=basin, basin_alt=basin_alt, report_date=report_date)
        filename = '{tmp_dir}/{report_date}.png'.format(
            tmp_dir=tmp_dir, report_date=report_date)
        # If we already have the file, no need to download it again.  Always grab the latest one though.
        if not os.path.isfile(filename) or 'latest' in filename:
            try:
                urllib.request.urlretrieve(url, filename)
            except urllib.error.HTTPError as ex:
                # Let the user know if one failed (some of them just aren't there, thanks NHC).
                print(
                    'Failed to download {file}: {error}'.format(file=url, error=ex))
                continue
        files.append(filename)

    # Output by basin given, perhaps later this can be user input.
    output_file = 'hurricane_5day_{basin}.gif'.format(basin=basin)

    # Use the Pillow library to generate the GIF from the files.  Optimize here
    # doesn't seem to do much.
    img, *imgs = [Image.open(f) for f in files]
    img.save(fp=output_file, format='GIF', append_images=imgs,
             save_all=True, duration=duration, loop=0, optimize=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--start-date', type=lambda s: datetime.datetime.strptime(
        s, '%Y-%m-%d'), help='If not specified, the current calendar year will be used.')
    parser.add_argument('-e', '--end-date', type=lambda s: datetime.datetime.strptime(s, '%Y-%m-%d'),
                        default=datetime.datetime.now(), help='If not specified, the current date and time will be used.  If --start-date is not specified, this is ignored.')
    parser.add_argument('-b', '--basin', default='atl',
                        choices=('atl', 'epac', 'cpac'), help='Basin to choose. atl=Atlantic, epac=Eastern Pacific, cpac=Central pacific (default: atl)')
    parser.add_argument('-d', '--duration', type=int, default=100,
                        help='Duration of each frame in milliseconds.')
    parser.add_argument('-f', '--frames-to-add', type=int, default=30,
                        help='Duplicates the final frame N times for a pause effect at the end (default: 30)')
    args = parser.parse_args()

    # If the user doesn't provide a start date, just use this entire year up to now.
    if not args.start_date:
        args.start_date = datetime.datetime(
            year=datetime.datetime.now().year, month=1, day=1)
        args.end_date = datetime.datetime.now()

    main(date_start=args.start_date, date_end=args.end_date,
         basin=args.basin, duration=args.duration, end_frames=args.frames_to_add)
